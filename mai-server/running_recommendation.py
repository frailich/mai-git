from pymongo import MongoClient
import numpy as np
import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer # Import CountVectorizer and create the count matrix
from sklearn.metrics.pairwise import cosine_similarity # Compute the Cosine Similarity matrix based on the count_matrix


import running_ridge as model

# db
# client = MongoClient("mongodb://cs707:2020CS707@193.106.55.107:80/")
# db = client['mai-test']
# db.collection_names()

# p = db["filtered_peoples"]
# m = db["filtered_movies"]
# g = db["genres"]

# p_length = p.estimated_document_count()
# g_length = g.estimated_document_count()
# m_length = m.estimated_document_count()

# movies_data = pd.DataFrame()
# overall_length = p_length+g_length+3

# for i, doc in enumerate(m.find({}), 0):
#     people_ids = [x['primaryName'] for x in doc['principals']]
#     #genres_ids = np.array(list(doc['genres'].values())) + p_length

#     row = {}
#     row['people'] = people_ids
#     row['genres'] = (list(doc['genres'].keys()))
#     row['startYear'] = doc['startYear']
#     row['runtimeMinutes'] = doc['runtimeMinutes']
#     row['averageRating'] = doc['averageRating']
#     row['numVotes'] = doc['numVotes']
#     row['primaryTitle'] = doc['primaryTitle']
    
#     movies_data = movies_data.append(row, ignore_index=True)

#     print(f"progress: {i+1}/{m_length}, {100*((i+1)/m_length)}" )

# # Calculate the minimum number of votes required to be in the chart, m
# m = movies_data['numVotes'].quantile(0.90)

# # Filter out all qualified movies into a new DataFrame
# movies_data = movies_data.copy().loc[movies_data['numVotes'] >= m]

# # Calculate mean of vote average column
# C = movies_data['averageRating'].mean()

# # Function that computes the weighted rating of each movie
# def weighted_rating(x, m=m, C=C):
#     v = x['numVotes']
#     R = x['averageRating']
#     # Calculation based on the IMDB formula
#     return (v/(v+m) * R) + (m/(m+v) * C)

# # Define a new feature 'score' and calculate its value with `weighted_rating()`
# movies_data['score'] = movies_data.apply(weighted_rating, axis=1)

# #Sort movies based on score calculated above
# movies_data = movies_data.sort_values('score', ascending=False)

# movies_data.to_json("movies_data.json")
movies_data = pd.read_json("movies_data.json")

# Function to convert all strings to lower case and strip names of spaces
def clean_data(x):
    if isinstance(x, list):
        return [str.lower(i.replace(" ", "")) for i in x]
    else:
        #Check if director exists. If not, return empty string
        if isinstance(x, str):
            return str.lower(x.replace(" ", ""))
        else:
            return ''

def create_soup(x):
    return ' '.join(x['genres']) + ' ' + ' '.join(x['people']) + ' ' + str(x['runtimeMinutes']) + ' ' +str(x['startYear'])

# Function that takes in movie title as input and outputs most similar movies
def get_recommendations(title, movies_data, indices, cosine_sim):
    # Get the index of the movie that matches the title
    idx = indices[title]

    # Get the pairwsie similarity scores of all movies with that movie
    sim_scores = list(enumerate(cosine_sim[idx]))

    # Sort the movies based on the similarity scores
    sim_scores = sorted(sim_scores, key=lambda x: x[1], reverse=True)

    # Get the scores of the 10 most similar movies
    sim_scores = sim_scores[1:11]

    # Get the movie indices
    movie_indices = [i[0] for i in sim_scores]

    # Return the top 10 most similar movies
    return movies_data.iloc[movie_indices]

def run(input_people, input_genres, input_publish_date, input_approximate_length_time, predicted_rating, input_movie_title, input_people_indices, input_genres_indices):

    movies_data_copy = movies_data.copy()
    
    # Add the current movie
    row = {}
    row['people'] = input_people
    row['genres'] = input_genres
    row['startYear'] = input_publish_date
    row['runtimeMinutes'] = input_approximate_length_time
    row['averageRating'] = ''
    row['numVotes'] = ''
    row['score'] = predicted_rating
    row['primaryTitle'] = input_movie_title

    movies_data_copy = movies_data_copy.append(row, ignore_index=True)

    # Clean data
    movies_data_copy['genres'] = movies_data_copy['genres'].apply(clean_data)
    movies_data_copy['people'] = movies_data_copy['people'].apply(clean_data)

    # Create a new soup feature
    movies_data_copy['soup'] = movies_data_copy.apply(create_soup, axis=1)

    # cosine similarity
    count = CountVectorizer(stop_words='english')
    count_matrix = count.fit_transform(movies_data_copy['soup'])

    cosine_sim = cosine_similarity(count_matrix, count_matrix)

    # Reset index of your main DataFrame and construct reverse mapping as before
    movies_data_copy = movies_data_copy.reset_index()

    # Get indices
    indices = pd.Series(movies_data_copy.index, index=movies_data_copy['primaryTitle'])

    # Get similar movies
    similar_movies = get_recommendations(input_movie_title, movies_data=movies_data_copy, indices=indices, cosine_sim=cosine_sim)
    
    # Get recommended people names
    overall_people = list()
    for index, movie in similar_movies.iterrows():
        movie_with_details = movies_data[(movies_data['primaryTitle'] == movie['primaryTitle']) & (movies_data['startYear'] == movie['startYear'])]
        overall_people = overall_people + movie_with_details['people'].values[0]

    # Get recommended people indices
    overall_people = list(set(overall_people))
    people_indices_similar = []
    with MongoClient("mongodb://cs707:2020CS707@193.106.55.107:80") as client:
        db = client['mai-test']
        # Get nID for recommended people
        people_documents = db['filtered_peoples_details'].find(
                {
                    "primaryName":{ "$in" :  overall_people}
                }, 
                {'_id': 0, 'nID': 1})

        for people_doc in people_documents:
            people_indices_similar.append(people_doc["nID"])

    # Run recommendation
    recommendation = []
    for specific_people_user in input_people_indices:
        new_people = input_people_indices.copy()
        new_people.remove(specific_people_user)
        for specific_people_similar in people_indices_similar:
            result = model.run(new_people + [specific_people_similar], input_genres_indices, input_publish_date, input_approximate_length_time)
            if (result > predicted_rating):
                recommendation.append({'result' : result, 'people_indices':  new_people + [specific_people_similar]})
            
    # Sort descending
    recommendation = sorted(recommendation,reverse=True, key=lambda k: k['result']) 
    if len(recommendation) > 0:
        return recommendation[0]
    return []



    
    
    