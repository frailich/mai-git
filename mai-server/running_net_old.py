from pymongo import MongoClient
from torch.utils.data import Dataset, DataLoader, random_split
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable

client = MongoClient("mongodb://cs707:2020CS707@193.106.55.107:80/")
db = client['mai-test']

class PredictScoreNet(nn.Module):
    '''
    This is our embedding class
    '''
    def __init__(self, layers_size, activation_functions):
        super(PredictScoreNet, self).__init__()

        
        layers = []
        for i in range(len(layers_size)-1):
            layer = nn.Linear(layers_size[i], layers_size[i+1])
            layers.append(layer)

        self.layers_and_funcs = list(zip(layers, activation_functions))
        self.net_modules = nn.ModuleList(layers)


    def forward(self, x):
        for layer, f in self.layers_and_funcs:
            x = layer(x)
            if f:
                x= f(x)
            
        return x

input_size = db['filtered_peoples'].count_documents({}) + db['genres'].count_documents({}) + 2 # 2 = startYear, runtimeMinutes

layers_size = [
    input_size,
    1000,
    250,
    10
]

# layers_size = [
#     ('', input_size, 100)
# ]

activations = [
    F.leaky_relu,
    F.leaky_relu,
    None
]

model = PredictScoreNet(layers_size, activations)#.cuda()
model.load_state_dict(torch.load('model.torch', map_location=torch.device('cpu')))

def run(people_indices, genre_indices, publish_date, approximate_length_time):
    people_onehot = all_one(db['filtered_peoples'].count_documents({}), people_indices, dtype=np.int16)
    genre_onehot = all_one(db['genres'].count_documents({}), genre_indices, dtype=np.int16)
    net_input = np.concatenate([
        people_onehot,
        genre_onehot,
        np.array([publish_date], dtype=np.int16),
        np.array([approximate_length_time], dtype=np.int16)], axis=0)

    with torch.no_grad():
        model.eval()
        
        result = model(torch.from_numpy(net_input).float())
        return torch.argmax(result).tolist()


def all_one(length, indices, dtype=np.float):
    '''
    Create one hot vector when all the indices is one
    '''
    l = np.zeros(length, dtype=dtype)
    l[list(indices)] = 1
    
    return l