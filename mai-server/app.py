#import os
import json
import pymongo
import running_ridge as model
import running_recommendation as recommender
import numpy as np
from flask import Flask
from flask_cors import CORS, cross_origin
from flask import request
import multiprocessing
app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'
from multiprocessing.dummy import Pool  # dummy uses threads


#usr = os.environ['MONGO_DB_USER']
#pwd = os.environ['MONGO_DB_PASS']
#client = pymongo.MongoClient("mongodb+srv://" + usr + ":" + pwd + "@firstcluster-obuqd.mongodb.net/test?retryWrites=true&w=majority")
client = pymongo.MongoClient("mongodb://cs707:2020CS707@193.106.55.107:80")
db = client['mai-test']

@app.route("/admin/", methods=['POST'])
@cross_origin()
def get_admin_data():
    try:
        predictions = db['predictions'].count()
        logins = db['logins'].count()

        return (json.dumps({"predictions" : predictions, "logins" : logins}), 200) # Return all predictions
    except Exception as e:
        print(e)
        return (e, 500)

@app.route("/predictions/", methods=['POST'])
@cross_origin()
def get_predictions():
    try:
        req_data = request.get_json()     
        predictions = list(db['predictions'].find({'email': req_data['email']}))
        for prediction in predictions:
            prediction['_id'] = ''

        return (json.dumps(predictions), 200) # Return all predictions
    except Exception as e:
        print(e)
        return (e, 500)

@app.route("/users/", methods=['GET'])
@cross_origin()
def get_users():
    try:
        return (json.dumps(list(db['users'].find({}, {'password' : 0}))), 200) # Return all users
    except Exception as e:
        print(e)
        return (e, 500)

@app.route("/users/<string:email>", methods=['POST'])
@cross_origin()
def insert_user(email):
    try:
        req_data = request.get_json()
        req_data['_id'] = email
        db['users'].insert_one(req_data) # Insert one user
        return (json.dumps(True), 200)
    except Exception as e:
        print(e)
        return (e, 500)

@app.route('/users/<string:email>', methods=['GET'])
@cross_origin()
def get_user(email):
    try:
        document = db['users'].find_one({'_id': email}, {'name' : 1})
        if document is None:
            return ('', 204)
        return (json.dumps(document), 200) # Return specific user
    except Exception as e:
        print(e)
        return (e, 500)
        
@app.route('/users/<string:email>', methods=['DELETE'])
@cross_origin()
def delete_user(email):
    try:
        deleted_count = db['users'].delete_one({'_id': email}).deleted_count # Delete one user
        if deleted_count == 1:
            return (json.dumps(True), 200) 
        return (json.dumps(False), 204)
    except Exception as e:
        print(e)
        return (e, 500)

@app.route('/users/<string:email>', methods=['PUT'])
@cross_origin()
def update_user(email):
    try:
        req_data = request.get_json()
        matched_count = db['users'].update_one({'_id':email}, { "$set": req_data }).matched_count # Update one user
        if matched_count == 1:
            return (json.dumps(True), 200)
        return (json.dumps(False), 204)
    except Exception as e:
        print(e)
        return (e, 500)

@app.route('/users/<string:email>/login', methods=['POST'])
@cross_origin()
def login_user(email):
    try:
        req_data = request.get_json()
        document = db['users'].find_one({'_id': email, 'password': req_data['password']}, {'password': 0}) # Login user
        if document is None:
            return ('', 401)
        
        db['logins'].insert_one({"email":email})
        return (json.dumps(document), 200)
    except Exception as e:
        print(e)
        return (e, 500)

@app.route('/peoples/notexist', methods=['POST'])
@cross_origin()
def peoples_notexist():
    try:
        req_data = request.get_json()
        actors = []
        writers = []
        producers = []

        if (req_data):
            if ('actors' in req_data.keys()):
                actors = req_data['actors']
            if ('writers' in req_data.keys()):
                writers = req_data['writers']
            if ('producers' in req_data.keys()):
                producers = req_data['producers']         
            
        actors_documents = db['filtered_peoples_details'].find(
            {
                "category":{
                    "$in":[
                        "actress",
                        "archive_footage",
                        "archive_sound",
                        "self",
	                    "actor"
                        ]
                },
                "primaryName":{ "$in" :  actors}
            }, 
            {'_id': 0, 'primaryName' : 1}); 

        producers_documents = db['filtered_peoples_details'].find(
            {
                "category":{
                    "$in":[
                        "director",
                        "producer",
                        "production_designer",
                        "cinematographer"
                        ]
                },
                "primaryName":{ "$in" : producers }
            }, 
            {'_id': 0, 'primaryName' : 1}); 

        writers_documents = db['filtered_peoples_details'].find(
            {
                "category":{
                    "$in":[
                        "editor",
                        "writer",
                        "composer"
                        ]
                },
                "primaryName":{ "$in" : writers }
            }, 
            {'_id': 0, 'primaryName' : 1}); 

        actors_mapped = []
        for actor_object in actors_documents:
            actors_mapped.append(actor_object["primaryName"])

        producers_mapped = []
        for producer_object in producers_documents:
            producers_mapped.append(producer_object["primaryName"])

        writers_mapped = []
        for writer_object in writers_documents:
            writers_mapped.append(writer_object["primaryName"])

        return ({"actors": json.dumps(np.setdiff1d(actors, list(actors_mapped)).tolist()), 
                "writers" : json.dumps(np.setdiff1d(writers, list(writers_mapped)).tolist()),
                "producers" : json.dumps(np.setdiff1d(producers, list(producers_mapped)).tolist())}, 200)
    except Exception as e:
        print(e)
        return (e, 500)


@app.route('/predict', methods=['POST'])
@cross_origin()
def predict():
    try:
        req_data = request.get_json()

        # Prepare parameters   
        people_documents = db['filtered_peoples_details'].find(
            {
                "primaryName":{ "$in" :  req_data["people"]}
            }, 
            {'_id': 0, 'nID': 1})

        people_indices = []
        for people in people_documents:
            people_indices.append(people["nID"])

        genre_documents = db['genres'].find(
            {
                "genre":{ "$in" :  req_data["genres"]}
            }, 
            {'_id': 0, 'index': 1})

        genre_indices = []
        for genre in genre_documents:
            genre_indices.append(genre["index"])
        
        publish_date = req_data['publish_date']
        approximate_length_time = req_data['approximate_length_time']

        # run net
        result = model.run(people_indices, genre_indices,publish_date, approximate_length_time)

        # Save prediction history
        req_data['result'] = result

        if ('recommendation' in req_data and req_data['recommendation'] is True):
            return ({
                "result" : result,
                **recommendation(req_data["genres"], req_data["people"], publish_date, approximate_length_time, result, req_data["name"], people_indices, genre_indices)
            }, 200)
        else:
           db['predictions'].insert_one(req_data)
           return ({"result" : result}, 200)
    except Exception as e:
        print(e.with_traceback())
        return (e, 500)


def recommendation(genres, people, publish_date, approximate_length_time, result, primaryTitle, people_indices, genre_indices):
    recommendation = recommender.run(people, genres, publish_date, approximate_length_time, result, primaryTitle, people_indices, genre_indices)

    if (len(recommendation) > 0):
        recommended_people_docs = db['filtered_peoples_details'].find(
        {
            "nID":{ "$in" : recommendation["people_indices"]}
        }, 
        {'_id': 0, 'primaryName': 1, 'category': 1})
        people_names = []
        for people in recommended_people_docs:
            people_names.append({"name": people["primaryName"], "role": people["category"]})
        # [(new_score, old_people_indices, new_people_indices), ....]

        return {'newScore' : str(recommendation['result']), 'people': people_names}
    else: 
        return {'newScore' : '' , 'people' : []}    










callback_results_brute = []
def recommendation_bruteforce(genre_indices, people_indices,publish_date,approximate_length_time, result):  
    callback_results_brute.clear()

    # Initialise pool of workers.
    pool = Pool(processes=8)
    batch_size = 1000     

    people_recommendation_docs = db['filtered_peoples_details'].find(
        {
            "nID":{ "$nin" :  people_indices}
        }, 
        {'_id': 0, 'nID': 1})

    people_all_recommendation_indices = []
    for current in people_recommendation_docs:
        people_all_recommendation_indices.append(current["nID"])

    tasks = []
    for current in people_indices:
        people_indices_copy = people_indices.copy()
        people_indices_copy.remove(current) # remove current
        matrix = []
        for i,curr_recommend in enumerate(people_all_recommendation_indices, 0): # run all recommendations
            matrix.append(people_indices_copy + [curr_recommend]) # append current recommendation
            if (i+1) % batch_size == 0 or (i+1) == len(people_all_recommendation_indices): # batch  
                tasks.append(pool.apply_async(
                            predictIteration,
                            (people_indices_copy, genre_indices, publish_date, approximate_length_time,batch_size,matrix.copy()),
                            callback=callback))
                matrix = []
    
    for task in tasks:
        task.wait()
    stam = [x["score"] for x in callback_results_brute]
    print(callback_results_brute[np.argmax([x["score"] for x in callback_results_brute])]) # best people indices 
    return callback_results_brute[np.argmax([x["score"] for x in callback_results_brute])] # TODO: NO ROUND

def predictIteration(people_indices, genre_indices,publish_date,approximate_length_time,batch_size,matrix):
    batch_result = model.run_batch(matrix, genre_indices,publish_date,approximate_length_time) # predict
    best_position = np.argmax(batch_result, axis=0) # best position
    return ({"score" : batch_result[best_position], "people_indices" : matrix[best_position]}) # append

def callback(result):
     callback_results_brute.append(result)
     print('iteration: {}'.format(len(callback_results_brute)))
     #print(callback_results_brute)

if __name__ == '__main__':
    app.run()