# Mai - Server

# For both install and run
env\Scripts\activate

# Install
pip install flask 
pip install flask-cors
pip install torch==1.5.0+cpu torchvision==0.6.0+cpu -f https://download.pytorch.org/whl/torch_stable.html
pip install xgboost
pip install sklearn
pip install pandas

# Run
set FLASK_APP=app.py
flask run

