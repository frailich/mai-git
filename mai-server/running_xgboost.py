from pymongo import MongoClient
from torch.utils.data import Dataset, DataLoader, random_split
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
import xgboost as xgb
from multiprocessing.dummy import Pool  # dummy uses threads

# db
client = MongoClient("mongodb://cs707:2020CS707@193.106.55.107:80/")
db = client['mai-test']
people_onehot_length = db['filtered_peoples'].count_documents({})
genre_onehot_length = db['genres'].count_documents({})

# Load model
model = xgb.Booster({'nthread': 8})  # init model
model.load_model('xgbModel.model')  # load data

def run(people_indices, genre_indices, publish_date, approximate_length_time):
    people_onehot = all_one(people_onehot_length, people_indices, dtype=np.int16)
    genre_onehot = all_one(genre_onehot_length, genre_indices, dtype=np.int16)
    net_input = np.concatenate([
        people_onehot,
        genre_onehot,
        np.array([publish_date], dtype=np.int16),
        np.array([approximate_length_time], dtype=np.int16)], axis=0).reshape((1,-1))
        
    # Predict results on test set
    return round(float(model.predict(xgb.DMatrix(net_input))[0]),1)

def run_batch(people_matrix, genre_indices, publish_date, approximate_length_time):
    # Initialise pool of workers.
    pool = Pool(processes=10)

    genre_onehot = all_one(genre_onehot_length, genre_indices, dtype=np.int16)
    publish_date = np.array([publish_date], dtype=np.int16)
    approximate_length_time = np.array([approximate_length_time], dtype=np.int16)

    tasks = []
    net_input = []
    for curr_people in people_matrix:
        tasks.append(pool.apply_async(concatenate, 
                         (net_input,
                         curr_people, 
                         genre_onehot,
                         publish_date, 
                         approximate_length_time)))
           
    for task in tasks:
        task.wait()

    # Predict results on test set
    return model.predict(xgb.DMatrix(np.array(net_input)))

def concatenate(net_input, curr_people, genre_onehot, publish_date, approximate_length_time):
    net_input.append(np.concatenate([
        all_one(people_onehot_length, curr_people, dtype=np.int16),
        genre_onehot,
        publish_date,
        approximate_length_time], axis=0))  

def all_one(length, indices, dtype=np.float):
    '''
    Create one hot vector when all the indices is one
    '''
    l = np.zeros(length, dtype=dtype)
    l[list(indices)] = 1
    
    return l