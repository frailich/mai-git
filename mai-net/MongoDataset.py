from torch.utils.data import Dataset, DataLoader
import numpy as np


    

def transform_doc_to_onehot(json):
    '''
    Transform a movie to onehot vector
    '''
    people_onehot = create_one_hot(
        people_onehot_length, [json['_id']], dtype=np.bool_)
    genre_onehot = create_one_hot(
        genres_onehot_length, json['value'].keys(), json['value'].values(), np.int8)

    return people_onehot, genre_onehot

class MongoDataset(Dataset):
    '''
    Custom Dataset to load the data from mongo
    '''
    def __init__(self, collection, transform=None, load_all_data_to_memory=False, pre_transform=False):
        self.transform = transform
        self.load_all_data_to_memory = load_all_data_to_memory
        self.pre_transform = pre_transform

        self.col = collection

        if not load_all_data_to_memory:
            self.ids = list(self.col.find({}, {'_id': True}))
        else:
            self.data = list(self.find())

            if pre_transform and transform:
                self.data = [ transform(doc) for doc in self.data ]

    def __len__(self):
        return self.col.estimated_document_count()
    
    def __getitem__(self, index):
        if not load_all_data_to_memory:
            sample = self.col.find_one(self.ids[index])
        else:
            sample = self.data[index]

        # if we have transform function and didnt load to memory
        # or have transform and load data to memory but didn't pre-transform the data
        if self.transform and ((self.load_all_data_to_memory and not self.pre_transform) or (not self.load_all_data_to_memory)):
            sample = self.transform(sample)

        return [sample]
    
class PeopleToGenresDataset(Dataset):
    def __init__(self, collection, transform=None):
        self.transform = transform
        self.col = collection
        
        self.size = self.col.estimated_document_count()
        
        self.peoples = []
        self.genres = []
        
        for doc in self.col.find():
            p, g = transform_doc_to_onehot(doc)
            self.peoples.append(p)
            self.genres.append(g)
        
    def __len__(self):
        self.size
        
    def __getitem__(self, index):
        return self.peoples[index], self.genres[index]