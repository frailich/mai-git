# To add a new cell, type '# %%'
# To add a new markdown cell, type '# %% [markdown]'
# %%
import pandas as pd


# %%
df = pd.read_csv('../data/title.basics.tsv', sep='\t', quoting=3)
# Filtering unnecessary data
df_movies = df[(df['titleType'] == 'movie') & (df['isAdult'] == 0)]


# %%
# Merging the movies with their ratings
df_rating = pd.read_csv('../data/title.ratings.tsv', sep='\t', quoting=3)
df_movie_w_rating = pd.merge(df_movies, df_rating, on='tconst', how='left')
df_movie_w_rating = df_movie_w_rating.dropna(axis=0, how='any', subset=['averageRating'])
df_movie_w_rating.count()


# %%
# Filtering the unnecessary principals
df_principals = pd.read_csv('../data/title.principals.tsv', sep='\t', quoting=3)
df_principals = df_principals[df_principals.tconst.isin(df_movie_w_rating.tconst)]
df_principals = df_principals.drop('characters',axis=1)


# %%
df_movie_w_rating.to_csv('../data/movies.csv',index=False)
df_principals.to_csv('../data/principals.csv',index=False)


# %%
import pandas as pd
df_movie_w_rating = pd.read_csv('../data/movies.csv')
df_principals = pd.read_csv('../data/principals.csv')


# %%
df_names = pd.read_csv('../data/name.basics.tsv', sep='\t', quoting=3).drop('deathYear',axis=1)
df_names.info()


# %%
# Merging the principals will the full person info
df_principals_w_names = pd.merge(df_principals, df_names, on='nconst', how='left')
df_principals_w_names.info()


# %%
# Converting the principals to json
df_principals_json = df_principals_w_names.groupby('tconst').apply(lambda x: x.to_json(orient='records'))


# %%
df_principals_json.to_csv('../data/principals-json.csv')


# %%
# Merging the principals into movies
df_principals_json_df = df_principals_json.to_frame("principals")
df_movies_final = pd.merge(df_movie_w_rating, df_principals_json_df, on='tconst', how='left')
df_movies_final = df_movies_final.drop('isAdult',axis=1)


# %%
df_movies_final = df_movies_final.drop('endYear',axis=1)
df_movies_final = df_movies_final[df_movies_final["startYear"] != '\\N']
df_movies_final.info()


# %%
df_movies_final.to_csv('../data/movies-final.csv',index=False)

# %% 
df_movies_final = pd.read_csv("../data/movies-final.csv")


# %%
df_movies_final.groupby("startYear").count().head(50)


# %%
df_big_70 = df_movies_final[pd.to_numeric(df_movies_final['startYear']) >= 1990]
df_big_70.count()

# %%
df_big_70[df_big_70['startYear'] == 1990]

# %%
