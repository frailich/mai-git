def all_one(length, indices, dtype=np.float):
    '''
    Create one hot vector when all the indices is one
    '''
    l = np.zeros(length, dtype=dtype)
    l[list(indices)] = 1
    
    return l

def splited_calc_weighted(length, indices, occurrences=None, dtype=np.float):
    '''
    Create one hot vector

    length - The length of the vector
    indices - List of indexes on the vector
    occurrences - list of occurences of each index
    '''
    l = np.zeros(length, dtype=dtype)
    indices = np.array(list(indices), dtype=0)
    
    if not occurrences:
        single_weight = 1/len(indices)
        l[indices] = single_weight
    else:
        single_weight = 1/sum(occurrences)
        l[indices] = np.array(list(occurrences), dtype=np.int8)*single_weight
        
    return l

def given_weights(length, indices, weights=None, dtype=np.float):
    '''
    Create one hot vector

    length - The length of the vector
    indices - List of indexes on the vector
    weights - list of weights of each index
    '''
    l = np.zeros(length, dtype=dtype)
    indices = np.array(list(indices), dtype=np.int8)
    
    if not weights:
        l[indices] = 1
    else:
        l[indices] = np.array(list(weights), dtype=np.int8)
        
    return l