import Vue from 'vue';
// import 'es6-promise/auto'
import Vuex from 'vuex'
import createPersistedState from "vuex-persistedstate";

Vue.use(Vuex);

export default new Vuex.Store({
    plugins: [createPersistedState()],
    state: {
        appbarForced: false,
        user: null,
        lastPredictionParams: null
    },
    getters: {
    },
    mutations: {
        setAppbarForced (state, value) {
            state.appbarForced = value;
        },
        setUser(state, user) {
            state.user = {
                email: user._id,
                name: user.name,
                isAdmin: user.isAdmin
            };
        },
        logout(state) {
            state.user = null;
        },
        lastPredictionParams(state, lastPrediction) {
            state.lastPredictionParams = lastPrediction;
        }
    }
})