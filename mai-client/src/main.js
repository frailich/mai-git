import Vue from 'vue';
import App from './App.vue';
import router from './router';

import vuetify from './plugins/vuetify';
import vuex_store from './plugins/vuex';
import '@babel/polyfill'
// import 'roboto-fontface/css/roboto/roboto-fontface.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'

Vue.config.productionTip = false;

new Vue({
  router,
  vuetify,
  store: vuex_store,
  render: (h) => h(App)
}).$mount('#app');
