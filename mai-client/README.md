# client

You need to use node version 12.14. You can install directly this version or you can use Node Version Manager (https://github.com/coreybutler/nvm-windows).
You need to install the packages with npm install command (under mai-client folder)
Then you can just run - start-dev.bat to serve the app for development.

## Project setup
```
npm install
```

## Fix linter problem
```
npm run lint --fix
```

### Compiles and hot-reloads for development - need to be in src directory in order to work
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

## Linter guide - 
install eslint VS code extension
add the following properties to settings.json (vs code personal settings file) : 
"eslint.format.enable": true,
    "editor.codeActionsOnSave": {
          "source.fixAll.eslint": true
    },
    "eslint.validate": [
        "javascript"
    ]

