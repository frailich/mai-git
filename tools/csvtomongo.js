const mongodb = require("mongodb").MongoClient;
const csvtojson = require("csvtojson");

let url = "mongodb://cs707:2020CS707@193.106.55.107:80/";

csvtojson()
  .fromFile("data-final.csv")
  .then(csvData => {
    csvData.forEach(function(row, index, object) {
        if (row.hasOwnProperty('principals') && row.principals != '') {
            row.principals = JSON.parse(row.principals);
        }
        else {
            object.splice(index, 1);
            console.log('deleted row: ' + index.toString());
        }
    })
    
    mongodb.connect(
      url,
      { useNewUrlParser: true, useUnifiedTopology: true },
      (err, client) => {
        if (err) throw err;

        client
          .db("mai")
          .collection("movies")
          .insertMany(csvData, (err, res) => {
            if (err) throw err;

            console.log(`Inserted: ${res.insertedCount} rows`);
            client.close();
          });
      }
    );
  });