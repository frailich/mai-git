@echo off
setlocal EnableDelayedExpansion

:sets
(SET "extract_folder=..\data\")
(SET "url=https://datasets.imdbws.com/")
(SET "files=name.basics.tsv.gz;title.akas.tsv.gz;title.basics.tsv.gz;title.crew.tsv.gz;title.episode.tsv.gz;title.principals.tsv.gz;title.ratings.tsv.gz")

:check_curl_exist
curl --help >nul 2>&1
if %errorlevel% neq 0 (
	ECHO curl doesn't exist, existing.
	GOTO end
)

:check_curl_exist
7z --help >nul 2>&1
if %errorlevel% neq 0 (
	ECHO 7z doesn't installed, or isn't in the environment variables Path, existing.
	GOTO end
)



:create_download_command
(SET "download_command=curl")
FOR %%x IN (!files!) DO (
	(SET "download_command=!download_command! -O %url%%%x")
)


:download_files
!download_command!


:extract_and_rename
FOR %%I IN (*.*) DO (
	IF "%%~xI" EQU ".gz" (
		7z e %%I -o%extract_folder%
		RENAME %extract_folder%data.tsv %%~nI
	)
)

:delete_zips
DEL /S *.gz

:end
endlocal
pause